/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.ByteArrayInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;

import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.opencv.core.MatOfInt;
import org.opencv.core.Size;
import static org.opencv.imgcodecs.Imgcodecs.imread;
import static org.opencv.imgcodecs.Imgcodecs.imwrite;

/**
 *
 *
 *
 *
 */
public class Teste {//Teste de COMMIT!!!////////
    // the FXML button

    @FXML
    private Button button;
    // the FXML image view

    @FXML
    public ImageView currentFrame;

    @FXML
    public TextField campo;
    @FXML
    public Slider escale;
    // the FXML image view

    // public ChoiceBox cb;
    private ScheduledExecutorService timer;
    // the OpenCV object that realizes the video capture
    private VideoCapture capture = new VideoCapture();
    private Mat img = imread("");
    private String caminho;
    private boolean imgActive = false;
    private Mat currentImg;

    /**
     * The action triggered by pushing the button on the GUI
     *
     * @param event the push button event
     */
    @FXML
    protected void Load(ActionEvent event) {//Carrega a um arquivo para visualização e edição
        this.caminho = campo.getText();
        this.img = imread(this.caminho);
        this.currentImg =img;
        //this.cb = new ChoiceBox(FXCollections.observableArrayList(
        //"First", "Second", "Third"));
        //this.cb.setItems(FXCollections.observableArrayList(
        //  "Níveis de Cinza", "HSV", "Canny"));
        System.out.println("CAMPO" + campo.getText());
        //System.out.println("CHOICE BOX" + cb.toString());
       /*Runnable frameGrabber = new Runnable() {

            @Override
            public void run() {
                Imgproc.resize(img, img, new Size(img.width() * escale.valueProperty().getValue(), img.width() * escale.valueProperty().getValue()));
                Image imageToShow = mat2Image(img);
                currentFrame.setImage(imageToShow);
                System.err.println("run");
            }
        };*/
     //  this.timer = Executors.newSingleThreadScheduledExecutor();
				//this.timer.scheduleAtFixedRate(frameGrabber, 0, 33, TimeUnit.MILLISECONDS);
       
        // Image imageToShow = mat2Image(img);
        //currentFrame.setImage(imageToShow);
        Image imageToShow = mat2Image(img);
                currentFrame.setImage(imageToShow);
                                this.imgActive = true;
        //cb.getSelectionModel().selectedIndexProperty().addListener(null);
        //this.timer = Executors.newSingleThreadScheduledExecutor();
        //this.timer.scheduleAtFixedRate(frameGrabber, 0, 33, TimeUnit.MILLISECONDS);
        /*try
         {
         this.timer.shutdown();
         this.timer.awaitTermination(33, TimeUnit.MILLISECONDS);
         }
         catch (InterruptedException e)
         {
				
         System.err.println("Exception in stopping the frame capture, trying to release the camera now... " + e);
         }

         // release the camera
         this.capture.release();
         // clean the frame
         this.currentFrame.setImage(null);*/
    }

    @FXML
    protected void Save(ActionEvent event) {//Salva um novo arquivo

        imwrite(this.caminho.substring(0, this.caminho.lastIndexOf(".")) + "-opencv.png", this.currentImg);
    }

    @FXML
    protected void ToGray(ActionEvent event) {// Converte para escala de cinza
        if (!this.imgActive) {
            Image imageToShow = mat2Image(img);;
            currentFrame.setImage(imageToShow);
            this.imgActive = true;
        }
        Mat aux = new Mat();
        Imgproc.cvtColor(img, aux, Imgproc.COLOR_BGR2GRAY);
        Image imageToShow = mat2Image(aux);;
        currentFrame.setImage(imageToShow);
          this.currentImg =aux;
        this.imgActive = false;
    }

    @FXML
    protected void ToHsv(ActionEvent event) {// Converte para HSV
        if (!this.imgActive) {
            Image imageToShow = mat2Image(img);;
            currentFrame.setImage(imageToShow);
            this.imgActive = true;
        }
        Mat aux = new Mat();
        Imgproc.cvtColor(img, aux, Imgproc.COLOR_RGB2HSV);
        Image imageToShow = mat2Image(aux);;
        currentFrame.setImage(imageToShow);
         this.currentImg =aux;
        this.imgActive = false;
    }

    @FXML
    protected void Canny(ActionEvent event) {// Aplica Canny
        if (!this.imgActive) {
            Image imageToShow = mat2Image(img);;
            currentFrame.setImage(imageToShow);
            
            this.imgActive = true;
        }
        Mat aux = new Mat();
        Imgproc.cvtColor(img, aux, Imgproc.COLOR_BGR2GRAY);
        Imgproc.Canny(img, aux, 5, 100);//pode ser criada barra para alterar valores
        Image imageToShow = mat2Image(aux);;
        currentFrame.setImage(imageToShow);
         this.currentImg =aux;
        this.imgActive = false;
    }

    @FXML
    protected void Resize(ActionEvent event) {//  Redimensiona a imagem
        Mat aux = new Mat();

        Imgproc.resize(img, img, new Size(img.size().width *0.5d, img.size().height *0.5d));
        System.out.println("tamanho da imagem"+ img.size().toString());
        //Imgproc.resize(img, aux, null);
        //Imgproc.Canny(img, img, 5, 100);//pode ser criada barra para alterar valores
        Image imageToShow = mat2Image(img);
         this.currentImg =img;
        currentFrame.setImage(imageToShow);
    }
    @FXML
    protected void Resize02(ActionEvent event) {//  Redimensiona a imagem
        Mat aux = new Mat();

        Imgproc.resize(img, img, new Size(img.size().width *2d, img.size().height *2d));

        //Imgproc.resize(img, aux, null);
        //Imgproc.Canny(img, img, 5, 100);//pode ser criada barra para alterar valores
        Image imageToShow = mat2Image(img);
         this.currentImg =img;
        currentFrame.setImage(imageToShow);
    }

    @FXML
    protected void Matriz(ActionEvent event) throws IOException {// Cria matriz a partir da imagem e salva em *txt
        int i = 0;
        Mat aux = new Mat();
        FileWriter arq = new FileWriter(this.caminho.substring(0, this.caminho.lastIndexOf(".")) + "-matriz.txt");
        PrintWriter gravarArq = new PrintWriter(arq);
        Imgproc.cvtColor(img, aux, Imgproc.COLOR_BGR2GRAY);
        MatOfInt imgInt = new MatOfInt();
        aux.copyTo(imgInt);
        

        double matriz[][] = new double[aux.height()][aux.width()];
        for (int linha = 0; linha < aux.height(); linha++) {
            for (int coluna = 0; coluna < aux.width(); coluna++) {
                double y[] = imgInt.get(linha, coluna);
                matriz[linha][coluna] = y[0];
                gravarArq.print(y[0] + " ");
               // System.out.println("Tamanho mat" + img.height() + "x" + img.width());
                //System.out.println("Tamanho int" + imgInt.size());
                //System.out.println("Tamanho y" + y.length);

            }

        }
        gravarArq.println();
    }

    @FXML
    protected void medianBlur(ActionEvent event) {// aplica medianBlur - tópico 5
        if (!this.imgActive) {
            Image imageToShow = mat2Image(img);;
            currentFrame.setImage(imageToShow);
            this.imgActive = true;
        }
        Mat aux = new Mat();
        Imgproc.cvtColor(img, aux, Imgproc.COLOR_BGR2GRAY);
        Imgproc.medianBlur(aux, aux, 5);
        Image imageToShow = mat2Image(aux);;
        currentFrame.setImage(imageToShow);
         this.currentImg =aux;
        this.imgActive = false;
    }

    @FXML
    protected void thresholding(ActionEvent event) {// aplica threshold - tópico 7
        if (!this.imgActive) {
            Image imageToShow = mat2Image(img);;
            currentFrame.setImage(imageToShow);
            this.imgActive = true;
        }
        Mat aux = new Mat();
        Imgproc.cvtColor(img, aux, Imgproc.COLOR_BGR2GRAY);
        Imgproc.threshold(aux, aux, 127, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C);
        Image imageToShow = mat2Image(aux);;
        currentFrame.setImage(imageToShow);
         this.currentImg =aux;
        this.imgActive = false;

    }

    /**
     *
     *
     */
    private Image grabFrame() {

        Image imageToShow = null;
        Mat frame = new Mat();

        if (this.capture.isOpened()) {
            try {
                this.capture.read(frame);

                if (!frame.empty()) {
                    // convert the image to gray scale
                    Imgproc.cvtColor(frame, frame, Imgproc.COLOR_BGR2GRAY);
                    // convert the Mat object (OpenCV) to Image (JavaFX)
                    imageToShow = mat2Image(frame);
                }

            } catch (Exception e) {
                // log the error
                System.err.println("Exception during the image elaboration: " + e);
            }
        }

        return imageToShow;
    }

    /**
     *
     *
     *
     */
    private Image mat2Image(Mat frame) {

        MatOfByte buffer = new MatOfByte();

        Imgcodecs.imencode(".png", frame, buffer);

        return new Image(new ByteArrayInputStream(buffer.toArray()));
    }

    public Mat getImage() {
        return this.img;
    }

}
