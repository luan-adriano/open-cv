

import org.opencv.core.Core;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.*;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.layout.Pane;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

public class Interface extends Application {
   
	@Override
	public void start(Stage primaryStage) {
		try {
			// load the FXML resource
			FXMLLoader loader = new FXMLLoader(getClass().getResource("InterfaceModel.fxml"));
			// store the root element so that the controllers can use it
			 
                       Pane rootElement = (Pane) loader.load();
                       	
                
            		
			//BorderPane rootElement = new BorderPane();
			//Button btn = new Button();
	        //btn.setText("Say 'Hello World'");
	        //rootElement.getChildren().add(btn);
			//BorderPane rootElement = (BorderPane) loader.load();
			Scene scene = new Scene(rootElement,600,400);
			scene.getStylesheets().add(getClass().getResource("interface.css").toExternalForm());
			primaryStage.setTitle("App OpenCV");
			primaryStage.setScene(scene);
			//primaryStage.centerOnScreen();
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
	// System.loadLibrary("opencv_java00") ;	
           //System.setProperty("java.library.path", System.getProperty("user.dir")+"\\");
           //String opencvpath = System.getProperty("user.dir") +"\\";
              //System.out.println("path"+System.getProperty("java.library.path"));           
//String libPath = System.getProperty("java.library.path");
          // System.load(opencvpath + Core.NATIVE_LIBRARY_NAME+".dll" );
         System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
               
		launch(args);
	}
}